import { Component, OnInit } from '@angular/core';
import results from '../assets/result.json'
import { GetApiService } from './get-api.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  checkTypeOf(a: any) {
    return typeof a;
  }
  title = 'Events-Page';
  // result: any = results;
  item: any;
  //Stores either room no or startUpName
  array: any = [];
  //for sequencing
  sum = 0;
  //Timestamp storing
  ts = [0]
  timestamps = [1]
  //For toggler
  videoPage: any;
  on = 0;
  //For one click only.
  flag = false;

  //Start Up Name
  startUpName: any;
  startUpNumber = 0;
  //control video
  video: any = document.getElementsByClassName('vid');
  logo: any = document.getElementById('logo')
  audioplayer = document.getElementById('vid') as HTMLMediaElement
  //Button Hover Animation

  //PlayButton
  playButton = true;
  disappear(){
    this.playButton = false;
  }

  videoLoad() {
    this.video[0].click();
  }
  playVideo() {
    this.video[0].play();
  }
  pauseVideo() {
    this.video[0].pause();
  }
  marginFrom = 0;
  marginTo = 0;
  keys:any;
  animationEnd: number = 1;

  //Progress tracks how much video has been played
  progress=0;
  //Count traverses the array.
  count=0;
  //Result stores the result.json file from API
  result: any;
  ngOnInit() {  
    // this.api.apiCall().subscribe(async (data)=>{
    //   console.log("get api data! Control is coming here.", data);
    //   await this.result;
    //   this.result = data;
    //   await this.keys 
    //   this.keys =  Object.keys(this.result)
    // });
    this.result = results;
    console.log(this.keys)
    // this.callAnimate(this.flag);
  };
  callAnimate(flag:boolean){
    if(!this.flag){
      this.animate();
      this.flag = true;
    }
  }
  hover = false;
  animate() {
    //Solution for autoplay problem by Kartik Bhaiya
    console.log(this.audioplayer)
    this.audioplayer = document.getElementById('vid') as HTMLMediaElement
    this.audioplayer.muted = false;
    // this.audioplayer.play();
    // this.audioplayer.loop = true;

    //for buttons - stores room no or startUp Name.
    this.array = [];
    
    //Timestamp storing
    this.ts = []

    //For toggler
    this.keys = Object.keys(this.result)

    //Creates an array containing the room no. or startUp name and a timestamp array from result.json file.
    for (let i = 0; i < 19; i++) {
      if (this.result[`${i + 1}`] != undefined) {
        this.sum += this.result[`${i + 1}`]["time"]
        this.result[`${i + 1}`]["time"] = this.sum;
        this.array[i] = this.result[`${i + 1}`]["startUp"]
        this.ts.push(this.sum)

      } else {
        this.array[i] = i + 1
      }
    }
    console.log(this.ts);

    //variable initialization
    this.count = 0;
    this.video[0].addEventListener('timeupdate',()=>{
      this.progress = this.video[0].currentTime;
    })
    let right=false;
    // MAIN LOGIC
    let anim = ()=>{
      
      // this.hover = !this.hover;
      
      //for testing purposes
      console.log(this.ts)
      console.log(this.result)
      
      //print startUpNumber on top-left and startUpName;
      this.startUpNumber = this.keys[this.count];
      this.startUpName = this.result[this.keys[this.count]]["startUp"]

      //Enter and open videoPage and play the video entirely.
      setTimeout(()=>{
        this.videoPage = true;
        this.video[0].play();
      },200)

      var a = setInterval(()=>{

        //For testing purposes
        console.log(this.progress);
        console.log(this.count)
        console.log(this.result[this.keys[this.count]]["startUp"])
        
        //If progress of video is more than timestamp count element, then close the videoPage and pause the video and increase count
        if(this.progress>this.ts[this.count]){
          //Clear the interval and increase the counter
          clearInterval(a);
          this.count++;
          //If count is equal to length of timestamp array, then array has been traversed completely, make count 0
          if(this.count==this.ts.length){
            this.videoPage = false;
            this.count = 0;
            this.pauseVideo();
            this.video[0].currentTime = 0;
            setTimeout(()=>{console.log("Video has fully played! We will close the veil now and replay")
            setTimeout(anim,1000);
          },500)
          }else{
            //call the anim function recursively.
            setTimeout(anim,1000);
          }
        }
        
        
      },100)
            
      if(this.count!=0){
        this.hover = !this.hover;
      }else{
        this.videoPage = false;
        this.pauseVideo();
      }
      //startUpNumber controls the hover animation over startUpNames.
      if (this.startUpNumber <= 10) {
        right = false;
        //if startUpNumber <= 10, no hover on the right side or vice versa.
        document.documentElement.style.setProperty('--block1', 'block')
        document.documentElement.style.setProperty('--block2', 'none')
        if (this.startUpNumber == 1) {
          this.marginTo = 5;
          document.documentElement.style.setProperty('--marginFrom', '0px')
          document.documentElement.style.setProperty('--marginTo', this.marginTo + 'px')
        } else {
          // this.marginFrom = (this.startUpNumber-2)*56 + 5
          document.documentElement.style.setProperty('--marginFrom', this.marginTo + 'px')
          this.marginTo = (this.startUpNumber - 1) * 56 + 5
          document.documentElement.style.setProperty('--marginTo', this.marginTo + 'px')
        }
      } else {
        if(right==false){
          this.marginTo = 0;
          right = true;
        }    
        document.documentElement.style.setProperty('--block2', 'block')
        document.documentElement.style.setProperty('--block1', 'none')
        if (this.startUpNumber == 11) {
          document.documentElement.style.setProperty('--marginFrom', '0px')
          document.documentElement.style.setProperty('--marginTo', '5px')
          this.marginTo = 5;
        } else {
          this.marginFrom = (this.startUpNumber-12)*56 + 5
          document.documentElement.style.setProperty('--marginFrom', this.marginTo + 'px')
          this.marginTo = (this.startUpNumber - 11) * 56 + 5
          document.documentElement.style.setProperty('--marginTo', this.marginTo + 'px')
        }
      }     
  
    }
    anim();

    //Old logic starts here.
    // //Creates a timestamps array that controls animation.
    // while (this.ts.length > 1) {
    //   this.timestamps.push((this.ts[1] - this.ts[0]) + this.timestamps[this.timestamps.length - 1])
    //   this.timestamps.push(this.timestamps[this.timestamps.length - 1] + 1)
    //   this.ts.shift();
    // }
    
    // //Removes the last element so the number of elements remains Odd, so animations starts with off and ends at off stage.
    // this.timestamps.pop();
    
    // //Animation controller using setTimeout- Calls a setTimeout for every element of timestamps array.
    
    // for (let i = 0; i < this.timestamps.length; i++) {
    //   setTimeout(() => {
    //     //Changes the state of videoPage variable for every element of timestamps array.
    //     this.videoPage = !this.videoPage;
    //     if (this.videoPage == true) {
    //       console.log(this.result[this.keys[this.on]]["startUp"])
    //       this.startUpName = this.result[this.keys[this.on]]["startUp"];
    //       this.startUpNumber = this.array.indexOf(this.startUpName) + 1;

    //       //startUpNumber controls the hover animation over startUpNames.
    //       if (this.startUpNumber <= 10) {
    //         //if startUpNumber <= 10, no hover on the right side or vice versa.
    //         document.documentElement.style.setProperty('--block1', 'block')
    //         document.documentElement.style.setProperty('--block2', 'none')
    //         if (this.startUpNumber == 1) {
    //           document.documentElement.style.setProperty('--marginFrom', '0px')
    //           document.documentElement.style.setProperty('--marginTo', '5px')
    //           this.marginTo = 5;
    //         } else {
    //           // this.marginFrom = (this.startUpNumber-2)*56 + 5
    //           document.documentElement.style.setProperty('--marginFrom', this.marginTo + 'px')
    //           this.marginTo = (this.startUpNumber - 1) * 56 + 5
    //           document.documentElement.style.setProperty('--marginTo', this.marginTo + 'px')
    //         }
    //       } else {
    //         document.documentElement.style.setProperty('--block2', 'block')
    //         document.documentElement.style.setProperty('--block1', 'none')
    //         if (this.startUpNumber == 11) {
    //           document.documentElement.style.setProperty('--marginFrom', '0px')
    //           document.documentElement.style.setProperty('--marginTo', '5px')
    //           this.marginTo = 5;
    //         } else {
    //           // this.marginFrom = (this.startUpNumber-12)*56 + 5
    //           document.documentElement.style.setProperty('--marginFrom', this.marginTo + 'px')
    //           this.marginTo = (this.startUpNumber - 11) * 56 + 5
    //           document.documentElement.style.setProperty('--marginTo', this.marginTo + 'px')
    //         }
    //       }
    //       this.on++;
    //       this.playVideo();
    //     } else {
    //       // console.log('Animation turned off at ' + this.timestamps[i] + 's')
    //       this.pauseVideo();
    //     }
    //   }, this.timestamps[i] * 1000)
    // }

    // setTimeout(() => {
    //   // location.reload();  
    //   this.animate();
    // }, (this.timestamps[this.timestamps.length - 1]+0.5) * 1000)
  }
  
  
  constructor(private api: GetApiService){
  }

}
