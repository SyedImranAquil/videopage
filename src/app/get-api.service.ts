import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
providedIn: 'root'})
export class GetApiService {

  constructor(private http:HttpClient) { }
  
  apiCall(){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
    };
    return this.http.get('https://videoproject0001.s3.us-west-2.amazonaws.com/result.json',httpOptions);
  }
}